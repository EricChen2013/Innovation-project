
// Innovation projectDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Innovation project.h"
#include "Innovation projectDlg.h"
#include "afxdialogex.h"
#include<iostream>
#include<stdlib.h>
#include<fstream>
#include<strstream>
using namespace std;
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CInnovationprojectDlg 对话框



CInnovationprojectDlg::CInnovationprojectDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInnovationprojectDlg::IDD, pParent)

{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CInnovationprojectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC8, m_result);
	//  DDX_Control(pDX, IDC_EDIT1, m_Path);
	DDX_Control(pDX, IDC_BUTTON1, m_test);
	//  DDX_Text(pDX, IDC_EDIT1, m_Path);
	DDX_Control(pDX, IDC_EDIT1, m_Path);
	DDX_Control(pDX, IDC_EDIT2, m_information);
}

BEGIN_MESSAGE_MAP(CInnovationprojectDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CInnovationprojectDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CInnovationprojectDlg::FileOpen)
	ON_BN_CLICKED(IDC_BUTTON3, &CInnovationprojectDlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// CInnovationprojectDlg 消息处理程序

BOOL CInnovationprojectDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标
	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CInnovationprojectDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CInnovationprojectDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CInnovationprojectDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CInnovationprojectDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_BUTTON1)->EnableWindow(0);
	CString cs("c:\\windows\\system32\\cmd.exe /c ping www.baidu.com >rt.txt");
	LPTSTR lpsz = (LPTSTR)(LPCTSTR)cs;
	SECURITY_ATTRIBUTES sa;
	HANDLE hRead,hWrite;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;
	if (!CreatePipe(&hRead,&hWrite,&sa,0))
	{
		MessageBox(_T("Error on CreatePipe()!"));
		return;
	}	
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	si.cb = sizeof(STARTUPINFO);
	GetStartupInfo(&si);
	si.hStdError = hWrite;
	si.hStdOutput = hWrite;
	si.wShowWindow = SW_HIDE;
	si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
	if (!CreateProcess(NULL,lpsz,NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi))
	{
		MessageBox(_T("Error on CreateProcess()!"));
		return;
	}
	while(1)
	{
	   if(WaitForSingleObject(pi.hThread,5000)==WAIT_OBJECT_0)
	   {
		   GetDlgItem(IDC_BUTTON1)->EnableWindow(1);
		   break; 
	   }
	}
	CloseHandle(hWrite);
    char ch,a[10]="100";
   	int yu=0,kai=0,shu,jinru=0;
	fstream ofice("rt.txt");
	for(int i=0;i<1000;i++) 
   {
	  ofice.get(ch);
	  if(ch=='%') jinru=1;
	}
	ofice.close();
	ofice.open("rt.txt");
	do
	{
	  if(jinru==0) break;
	  ofice.get(ch);
	  if(ch=='%') kai =0;
	  if(kai==1)
	  {
		a[yu]=ch;
		yu++;
	  }
	  if(ch=='(') kai=1;
	}while(yu!=2);
	ofice.close();
	shu=atoi(a);
	if(shu<100) m_result.SetWindowTextW(_T("网络通畅"));
	else m_result.SetWindowTextW(_T("无法连接到网络"));
	MessageBox(_T("测试结束"));
}


void CInnovationprojectDlg::FileOpen()
{
	// TODO: 在此添加控件通知处理程序代码
  CFileDialog cfd(true,_T(".jpg"),NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,_T("Executable   Files   (*.mp3)|*.mp3|All   Files   (*.*)|*.*||"),this);     
  if(cfd.DoModal()==IDOK)     
 {   
	 file=cfd.GetPathName(); 
	 m_Path.SetWindowTextW(file); 
  }
}


void CInnovationprojectDlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_BUTTON3)->EnableWindow(0);
	CString cs;
	cs="c:\\windows\\system32\\cmd.exe /c python lookup.py ";
	CString yt;
	yt=" >res.txt";
	cs=cs+file+yt;
	LPTSTR lpsz = (LPTSTR)(LPCTSTR)cs;
	SECURITY_ATTRIBUTES sa;
	HANDLE hRead,hWrite;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;
	if (!CreatePipe(&hRead,&hWrite,&sa,0))
	{
		MessageBox(_T("Error on CreatePipe()!"));
		return;
	}
	
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	si.cb = sizeof(STARTUPINFO);
	GetStartupInfo(&si);
	si.hStdError = hWrite;
	si.hStdOutput = hWrite;
	si.wShowWindow = SW_HIDE;
	si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
	if (!CreateProcess(NULL,lpsz,NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi))
	{
		MessageBox(_T("Error on CreateProcess()!"));
		return;
	}
	while(1)
	{
	   if(WaitForSingleObject(pi.hThread,5000)==WAIT_OBJECT_0)
	   {
		   GetDlgItem(IDC_BUTTON3)->EnableWindow(1);
		   break; 
       }
	}
	CloseHandle(hWrite);
	char ch,df[1000];
	int bi=0,ji,i;
	fstream ofice;
	ofice.open("res.txt");
	for(i=0;i<1000;i++)
	{
			ofice.get(ch);
			if(ch=='\n') bi++;
			else bi=0;
			if(bi>1)
			{
				df[i]='\0';
				break;
			}
			df[i]=ch;
	}
	CString ser,rts;
	ji=i;
	for(i=0;i<ji;i++)
	{
		if(df[i]=='\n') rts="\r\n";
		else rts=df[i];
		ser+=rts;
	}
	ofice.close();
	m_information.SetWindowTextW(ser);
	MessageBox(_T("检索结束"));
}
