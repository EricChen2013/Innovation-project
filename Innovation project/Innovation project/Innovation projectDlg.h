
// Innovation projectDlg.h : 头文件
//

#pragma once 
#include "afxwin.h"
//#pragma comment(linker, "/subsystem:\"windows\"   /entry:\"wWinMainCRTStartup\"")  
//#pragma comment( linker, "/subsystem:windows /entry:WinMainCRTStartup" )
//#pragma comment( linker, "/subsystem:windows /entry:mainCRTStartup" )

//#pragma comment( linker, "/subsystem:console /entry:mainCRTStartup" )
//#pragma comment( linker, "/subsystem:console /entry:WinMainCRTStartup" )

// CInnovationprojectDlg 对话框
class CInnovationprojectDlg : public CDialogEx
{
// 构造
public:
	CInnovationprojectDlg(CWnd* pParent = NULL);	// 标准构造函数
	CString file;
// 对话框数据
	enum { IDD = IDD_INNOVATIONPROJECT_DIALOG };
    HANDLE hRead,hWrite;
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	CStatic m_result;
	afx_msg void FileOpen();
//	CEdit m_Path;
	CButton m_test;
	afx_msg void OnBnClickedButton3();
//	CString m_Path;
	CEdit m_Path;
	CEdit m_information;
};
